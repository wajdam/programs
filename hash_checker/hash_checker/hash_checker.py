#!/usr/bin/env python3.7
from subprocess import check_output
from os import walk
from colorama import init
from termcolor import colored
from os import system
from sys import exit

HASH_METHODS = ["md5sum", "sha256sum"]
FILES = [f for r, d, f in walk("../..", topdown=False)][-1]


def clear_screen():
    system("clear")


def verify_expected_value(value, _from, to):
    if str(value).isdigit():
        if _from <= int(value) < to:
            return True
    exit("Unexpected value. Try one more time.")


def start_new_print(string=""):
    clear_screen()
    print("\n" * 2)
    print("#" * 20)
    print("\n")
    print(string)


def get_method_from_user():
    start_new_print("Hash methods")
    for idx, method in enumerate(HASH_METHODS):
        print(str(idx)+" -->", method)
    print("\n"*2)
    which_method = input("Chose method: ")
    verify_expected_value(_from=0, to=len(HASH_METHODS), value=which_method)
    return HASH_METHODS[int(which_method)]


def get_hash_from_user():
    start_new_print()
    hsh = input("Input hash string: ")
    return hsh


def get_file_path_from_user():
    start_new_print("Files:")
    for idx, file in enumerate(FILES):
        print(str(idx)+" -->", file)
    print("\n" * 2)
    which_file = input("Chose file: ")
    verify_expected_value(_from=0, to=len(FILES), value=which_file)
    return FILES[int(which_file)]


def main():
    method = get_method_from_user()
    output = check_output([method, get_file_path_from_user()])
    hash_sum = output.decode("utf8").split(" ")[0]
    user_hash = get_hash_from_user()

    start_new_print("Used method: {}".format(method))
    print()
    print("File's hash:\t", hash_sum)
    print("User's hash:\t", user_hash)

    init()

    print("\n")
    if hash_sum == user_hash:
        print(colored('SUCCESS', 'white', 'on_green'))
    else:
        print(colored('FAIL', 'white', 'on_red'))
    print("\n" * 2)


if __name__ == '__main__':
    main()
