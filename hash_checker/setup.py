from setuptools import setup

setup(name='hash_checker',
      version='0.1',
      description='Application to make openssl more convenient than in the standard application',
      url='https://bitbucket.org/wajdam/programs/src/master/',
      author='Marcel Wajda',
      author_email='marcelwajda@gmail.com',
      license='MIT',
      packages=[],
      zip_safe=False)
