#!/usr/bin/env python3.7
from .file_finder import main_executor

if __name__ == '__main__':
    main_executor()
