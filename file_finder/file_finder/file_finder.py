#!/usr/bin/env python3.7
"""
The script is a tool to find file patterns in defined date range.

commandline arguments:
--path <path_to_directory_where_you_want_to_start_to_search>

Example: python __main__.py --path C:\

"""
from pathlib import Path
from datetime import datetime
from collections import namedtuple
import argparse
from sys import exit


def get_time_range():
    try:
        print()
        print("Input start date")

        f_year = int(input("year: "))
        f_month = int(input("month: "))
        f_day = int(input("day: "))
        f_hour = int(input("hour: "))
        f_minute = int(input("minute: "))
        f_second = int(input("second: "))

        print()
        print("Input end date")

        t_year = int(input("year: "))
        t_month = int(input("month: "))
        t_day = int(input("day: "))
        t_hour = int(input("hour: "))
        t_minute = int(input("minute: "))
        t_second = int(input("second: "))

        from_date = int(datetime(year=f_year, month=f_month, day=f_day,
                                 hour=f_hour, minute=f_minute, second=f_second,
                                 microsecond=0).timestamp())
        to_date = int(datetime(year=t_year, month=t_month, day=t_day,
                               hour=t_hour, minute=t_minute, second=t_second,
                               microsecond=0).timestamp())
        collection = namedtuple("date_range", "from_date to_date")
        collection.from_date = from_date
        collection.to_date = to_date
        return collection
    except ValueError:
        exit("Incorrect value. Check what are expected date time values and try again.")


def get_file_pattern():
    print()
    print("Input pattern.")
    print("For example: *.png -> to get all .png files, or photo* -> to get all files started with photo")
    pattern = input("Pattern: ")
    return pattern


def main(cmd_args):
    date_range = get_time_range()
    pattern = get_file_pattern()
    counter = 0
    print()
    print("Find {} files from {} to {}".format(
        pattern,
        datetime.fromtimestamp(date_range.from_date),
        datetime.fromtimestamp(date_range.to_date)))
    print("List of files: ")
    for filename in Path(cmd_args.path).rglob(pattern):
        modification_time = int(filename.stat().st_mtime)
        if modification_time in range(date_range.from_date, date_range.to_date):
            print(datetime.fromtimestamp(modification_time).__str__()+"\t", filename.absolute())
            counter += 1

    print()
    if not counter:
        print("There is no any file pattern in given date range.")
    else:
        print("Number of founded files: {}".format(counter))
    print()


def get_args():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--path', metavar='p', type=str,
                        help='Path to search', required=True)
    _args = parser.parse_args()
    return _args


def main_executor():
    args = get_args()
    print()
    print()
    print("APPLICATION START")
    main(cmd_args=args)
    print("APPLICATION END")
    print()
    print()


if __name__ == '__main__':
    main_executor()
