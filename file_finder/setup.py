from setuptools import setup

setup(name='file_finder',
      version='0.1',
      description='Application to finding files',
      url='https://bitbucket.org/wajdam/programs/src/master/',
      author='Marcel Wajda',
      author_email='marcelwajda@gmail.com',
      license='MIT',
      packages=[],
      zip_safe=False)
