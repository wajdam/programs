from pyaudio import paInt24
from play import Play
from record import Record
import pyxhook
from time import sleep
from os import system


class LoopInput(object):
    def __init__(self, key, record, play):
        self.key = key
        self.record = record
        self.play = play


class LooperStation(object):
    INPUTS = [LoopInput(key=str(i),
                        record=Record(channels=1,
                                      chunk=1024,
                                      rate=44100,
                                      filename='input_{}.wav'.format(i),
                                      data_format=paInt24),
                        play=Play(chunk=1024,
                                  filename='input_{}.wav'.format(i)))
              for i in range(1, 10)]

    def __init__(self):
        self.hm = pyxhook.HookManager()
        self.hm.KeyDown = self.on_key_board_event
        self.hm.HookKeyboard()
        self.hm.start()
        self.hm_stop = False
        while not self.hm_stop:
            sleep(0.1)
        self.hm.cancel()

    def print_inputs_status(self):
        system("clear")
        print("I\tVALUE\tREADY")
        for loop_input in self.INPUTS:
            print(loop_input.key, '\t', not loop_input.play.is_paused, '\t', loop_input.record.was_executed)

    def on_key_board_event(self, event):
        if event.Key == "0":
            for loop_input in self.INPUTS:
                loop_input.play.stop()
                loop_input.record.stop()
            for loop_input in self.INPUTS:
                try:
                    loop_input.play.join()
                except RuntimeError:
                    pass
                try:
                    loop_input.record.join()
                except RuntimeError:
                    pass
            self.hm_stop = True

        for loop_input in self.INPUTS:
            if event.Key == loop_input.key:
                record = loop_input.record
                play = loop_input.play
                if record.is_stopped and play.is_stopped:
                    record.start()
                elif not record.is_stopped and play.is_stopped:
                    record.stop()
                    play.start()
                    self.print_inputs_status()
                elif record.is_stopped and not play.is_stopped:
                    play.is_paused = not play.is_paused
                    self.print_inputs_status()
                else:
                    record.stop()
                    play.stop()
                break
