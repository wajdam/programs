import pyaudio
import wave
from threading import Thread


class Record(Thread):
    def __init__(self, chunk, rate, data_format, channels, filename):
        Thread.__init__(self)
        self.is_stopped = True
        self.chunk = chunk
        self.rate = rate
        self.data_format = data_format
        self.channels = channels
        self.filename = filename
        self.was_executed = False

    def run(self):
        self.is_stopped = False
        p = pyaudio.PyAudio()

        stream = p.open(format=self.data_format,
                        channels=self.channels,
                        rate=self.rate,
                        input=True,
                        frames_per_buffer=self.chunk)

        print("* recording")

        frames = []

        while not self.is_stopped:
            data = stream.read(self.chunk)
            frames.append(data)

        print("* done recording")

        stream.stop_stream()
        stream.close()
        p.terminate()

        wf = wave.open(self.filename, 'wb')
        wf.setnchannels(self.channels)
        wf.setsampwidth(p.get_sample_size(self.data_format))
        wf.setframerate(self.rate)
        wf.writeframes(b''.join(frames))
        wf.close()
        self.was_executed = True

    def stop(self):
        self.is_stopped = True
