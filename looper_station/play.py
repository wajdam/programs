import pyaudio
import wave
from threading import Thread
from time import sleep

class Play(Thread):
    def __init__(self, chunk, filename):
        Thread.__init__(self)
        self.chunk = chunk
        self.filename = filename
        self.is_stopped = True
        self.is_paused = True

    def run(self):
        self.is_stopped = False
        self.is_paused = False
        wf = wave.open(self.filename, 'rb')
        p = pyaudio.PyAudio()

        stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                        channels=wf.getnchannels(),
                        rate=wf.getframerate(),
                        output=True)

        data = wf.readframes(self.chunk)
        while not self.is_stopped:
            data = b''
            sleep(0.01)
            while not self.is_paused:
                if data == b'':
                    wf = wave.open(self.filename, 'rb')
                    data = wf.readframes(self.chunk)
                stream.write(data)
                data = wf.readframes(self.chunk)

        stream.stop_stream()
        stream.close()
        p.terminate()

    def stop(self):
        self.is_stopped = True
        self.pause()

    def pause(self):
        self.is_paused = True

    def rerun(self):
        self.is_paused = False
