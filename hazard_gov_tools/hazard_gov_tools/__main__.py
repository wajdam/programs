#!/usr/bin/env python3.7

import xml.etree.ElementTree as ET
from requests import get


URL = "https://hazard.mf.gov.pl/api/Register"
FILE = "hazard.xml"

data = get(URL)

with open(FILE, 'w') as f:
    f.write(data.text)

mytree = ET.parse(FILE)
myroot = mytree.getroot()

print(myroot)
