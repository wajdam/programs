from setuptools import setup

setup(name='micropython_http_controller',
      version='0.1',
      description='Application to parse Polish hazard gov web page',
      url='https://bitbucket.org/wajdam/programs/src/master/',
      author='Marcel Wajda',
      author_email='marcelwajda@gmail.com',
      license='MIT',
      packages=[],
      zip_safe=False)
