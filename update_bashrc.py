"""
Script adds to the $HOME/.bashrc each directory on its level if the directory doesn't contain '.' sign at the beginning.

"""

from pathlib import Path


def main():
    main_path = Path(Path.cwd().__str__())
    main_path_iterator = main_path.iterdir()
    paths_list = []
    while True:
        try:
            path = main_path_iterator.__next__()
            if path.is_dir() and not path.__str__().split("/")[-1].startswith("."):
                command = 'export PATH="{}:$PATH"'.format(path.__str__())
                paths_list.append(command)
        except StopIteration:
            break

    exclude_from_update = set()
    with open(Path.home().__str__()+"/.bashrc", "r") as f:
        for line in f.readlines():
            for idx, command in enumerate(paths_list):
                if line.__contains__(command):
                    exclude_from_update.add(idx)

    if not len(exclude_from_update) == len(paths_list):
        with open(Path.home().__str__()+"/.bashrc", "a") as f:
            for idx, command in enumerate(paths_list):
                if not exclude_from_update.__contains__(idx):
                    f.write(command+"\n")


if __name__ == '__main__':
    # main()
    print("blocked")
